section .text
%define READ 0
%define WRITE 1

%define EXIT_SIG 60

%define STDOUT 1
%define STDIN 0

%define SPACE ' '
%define ASCII_TAB `\t`
%define NEW_LINE `\n`


; Принимает код возврата и завершает текущий процесс
exit:   
    mov rax, EXIT_SIG
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    mov rax, WRITE
    pop rsi
    mov rdi, STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_LINE

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, WRITE
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1 ; byte size
    syscall
    pop rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, 0x2D
    call print_char
    pop rdi
    neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
print_uint:
    push rbp
    mov rbp, rsp
    mov rax, rdi
    mov rcx, 10
    dec rsp             
    mov byte[rsp], 0   
    .loop:
        xor rdx, rdx      
        div rcx           
        add dl, '0'       
        dec rsp   
        mov byte[rsp], dl 
        test rax, rax      
        jnz .loop         
    mov rdi, rsp     
    call print_string
    mov rsp, rbp
    pop rbp
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi - указатель на первую строку
; rsi - указатель на вторую строку
; rcx - offset
string_equals:
    xor rcx, rcx
    .loop:
        mov bl, [rdi + rcx]
        cmp bl, byte[rsi + rcx]
        jne .not_equal
        test bl, bl
        je .equal
        inc rcx
        jmp .loop
    .equal:
        mov rax, 1
        ret
    .not_equal:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor     rax, rax
    xor     rdi, rdi
    mov     rdx, 1     ; 1 byte
    dec     rsp             
    mov     rsi, rsp
    syscall
    test al, al
    jz .exit
    mov al, [rsp]
    .exit:
        inc rsp
        ret 

        

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская 0x20, 0x9, 0xA в начале.
; Останавливается и возвращает 0 если слово слишком большое для буфера

; При успехе возвращает адрес буфера в rax, длину слова в rdx.

; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    test rsi, rsi
    jz .zero_length

    push r12
    push r13
    push r14

    mov r12, rdi
    mov r13, rsi
    xor r14, r14

    dec r13
    .whitespace:
        call read_char
        cmp al, SPACE
        je .whitespace
        cmp al, NEW_LINE
        je .whitespace
        cmp al, ASCII_TAB
        je .whitespace

    .loop:
        cmp  r14, r13
        je .fail
        test al, al
        jz .success
        cmp al, NEW_LINE
        je .success
        cmp al, SPACE
        je .success
        cmp al, ASCII_TAB
        je .success

        mov [r12+r14], al
        inc r14
        call read_char
        jmp .loop

    .success:
        mov byte[r12 + r14], 0
        mov rax, r12
        mov rdx, r14
        jmp .return

    .fail:
        xor rax, rax

    .return:
        pop r14
        pop r13
        pop r12
        ret

    .zero_length:
        xor rax, rax
        ret 


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax        
    xor rdx, rdx        
    cmp byte[rdi], '-' 
    je .signed         
    cmp byte[rdi], '+' 
    jne .unsigned       
    inc rdi             
    jmp .unsigned       
.signed:
    inc rdi             
    call parse_uint      
    neg rax             
    inc rdx             
    ret                     
.unsigned:
                

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push r12             
    push r13             
    xor rax, rax        
    xor r13, r13        
    mov r12, 10         ; Set base to 10
    xor rdx, rdx        
.loop:
    movzx rcx, byte[rdi + r13] 
    test rcx, rcx             
    jz .success             
    cmp rcx, '0'            
    jb .success             
    cmp rcx, '9'
    ja .success
    sub rcx, '0'            ; Convert character to digit
    mul r12                  ; Multiply by base
    add rax, rcx             ; Add digit to result
    inc r13                  
    jmp .loop                
.success:
    mov rdx, r13             
    pop r13                  
    pop r12                  
    ret                      


; Принимает указатель на null-terminated строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
        cmp rax, rdx
        jb .loop_body
        jae .fail
    .loop_body:
        mov cl, byte[rdi + rax] 
        mov byte[rsi + rax], cl
        inc rax
        test cl, cl
        jz .success
        jmp .loop
    .fail:
        xor rax, rax
    .success:
        ret
